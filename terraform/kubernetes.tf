resource "kubernetes_namespace" "apache" {
  metadata {
    name = "apache"
  }
}

resource "kubernetes_deployment" "apache" {
  metadata {
    name      = "apache"
    namespace = kubernetes_namespace.apache.metadata.0.name
  }
  spec {
    replicas = 2
    selector {
      match_labels = {
        app = "MyTestApp"
      }
    }
    template {
      metadata {
        labels = {
          app = "MyTestApp"
        }
      }
      spec {
        container {
          image = "httpd:2.4"
          name  = "apache-container"
          port {
            container_port = 80
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "apache" {
  metadata {
    name      = "apache"
    namespace = kubernetes_namespace.apache.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.apache.spec.0.template.0.metadata.0.labels.app
    }
    type = "LoadBalancer"
    port {
      port        = 80
      target_port = 80
    }
  }
}

resource "kubernetes_service_account" "fluentd" {
  metadata {
    name = "fluentd"
    namespace = "kube-system"
  }
}

resource "kubernetes_cluster_role" "fluentd" {
  metadata {
    name = "fluentd"
  }
  rule {
    api_groups = [""]
    resources  = ["namespaces", "pods"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "fluentd" {
  metadata {
    name = "fluentd"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "fluentd"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "fluentd"
    namespace = "kube-system"
  }
}

resource "kubernetes_deployment" "fluentd" {
  metadata {
    name      = "fluentd"
    namespace = "kube-system"
  }
  spec {
    selector {
      match_labels = {
        k8s-app = "fluentd-logging"
      }
    }
    template {
      metadata {
        labels = {
          k8s-app = "fluentd-logging"
        }
      }
      spec {
        service_account_name = kubernetes_service_account.fluentd.metadata.0.name
        container {
          image = "fluent/fluentd-kubernetes-daemonset:v1.3-debian-elasticsearch"
          name  = "fluentd"
          env {
            name  = "FLUENT_ELASTICSEARCH_HOST"
            value = "https://search-example-qyetdkewzyrlnk6pffu24aerpu.eu-central-1.es.amazonaws.com"
          }
          env {
            name  = "FLUENT_ELASTICSEARCH_PORT"
            value = "9200"
          }
          env {
            name  = "FLUENT_ELASTICSEARCH_SCHEME"
            value = "https"
          }
          env {
            name  = "FLUENT_UID"
            value = "0"
          }
        }
      }
    }
  }
}


resource "kubernetes_cluster_role" "reader" {
  metadata {
    name = "reader"
  }
  rule {
    api_groups = [""]
    resources  = ["deployments", "configmaps", "pods", "secrets", "services"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "reader" {
  metadata {
    name = "reader"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "reader"
  }
  subject {
    kind      = "Group"
    name      = "reader"
    api_group = "rbac.authorization.k8s.io"
  }
}

