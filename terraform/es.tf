resource "aws_elasticsearch_domain" "example" {
  domain_name           = "example"
  elasticsearch_version = "7.10"
  cluster_config {
    instance_type = "r4.large.elasticsearch"
  }
  advanced_security_options {
    enabled                        = true
    internal_user_database_enabled = true
    master_user_options {
      master_user_name     = "admin"
      master_user_password = "Password1+"
    }
  }
  encrypt_at_rest {
    enabled    = true
  }
  ebs_options {
    ebs_enabled = true
    volume_size = 10
  }
  domain_endpoint_options {
    enforce_https                   = true
    tls_security_policy             = "Policy-Min-TLS-1-0-2019-07"
  }
  node_to_node_encryption {
    enabled = true
  }
  tags = {
    Domain = "TestDomain"
  }
}
